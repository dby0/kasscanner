package uk.divisiblebyzero.kasscraper;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import uk.divisiblebyzero.kasscraper.config.SpringConfig;
import uk.divisiblebyzero.kasscraper.service.KasScraperService;

/**
 * Created by: Matthew Smalley
 * Date: 20/12/2015
 */
public class Launcher {
    public static void main(String[] args) {
        ApplicationContext ctx =
                new AnnotationConfigApplicationContext(SpringConfig.class);

        KasScraperService kasScraperService = ctx.getBean(KasScraperService.class);
        kasScraperService.scrape();
    }
}
