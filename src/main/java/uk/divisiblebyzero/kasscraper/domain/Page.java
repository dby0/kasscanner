package uk.divisiblebyzero.kasscraper.domain;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by: Matthew Smalley
 * Date: 20/12/2015
 */
public class Page {
    private String title;
    private URL originalAddress;
    private String parentCategory;
    private String contents;
    private URL newAddress;
    private String destinationId;
    private Map<String, byte[]> images = new HashMap<>();

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public URL getOriginalAddress() {
        return originalAddress;
    }

    public void setOriginalAddress(URL originalAddress) {
        this.originalAddress = originalAddress;
    }

    public String getParentCategory() {
        return parentCategory;
    }

    public void setParentCategory(String parentCategory) {
        this.parentCategory = parentCategory;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public URL getNewAddress() {
        return newAddress;
    }

    public void setNewAddress(URL newAddress) {
        this.newAddress = newAddress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Page page = (Page) o;

        return title.equals(page.title);

    }

    @Override
    public String toString() {
        return "Page{" +
                "title='" + title + '\'' +
                ", originalAddress=" + originalAddress +
                ", parentCategory='" + parentCategory + '\'' +
                ", destinationId='" + destinationId + '\'' +
                '}';
    }

    @Override
    public int hashCode() {
        return title.hashCode();
    }

    public void setDestinationId(String destinationId) {
        this.destinationId = destinationId;
    }

    public String getDestinationId() {
        return destinationId;
    }

    public Map<String, byte[]> getImages() {
        return images;
    }

    public void setImages(Map<String, byte[]> images) {
        this.images = images;
    }
}
