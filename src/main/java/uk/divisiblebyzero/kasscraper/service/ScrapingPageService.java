package uk.divisiblebyzero.kasscraper.service;

import org.ccil.cowan.tagsoup.jaxp.SAXParserImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import uk.divisiblebyzero.kasscraper.domain.Page;

import javax.annotation.Resource;

/**
 * Created by: Matthew Smalley
 * Date: 20/12/2015
 */
@Service
public class ScrapingPageService implements PageService {
    private final static Logger LOGGER = LoggerFactory.getLogger(ScrapingPageService.class);

    @Resource
    private Comms comms;

    @Resource
    private PageSaxHandler pageSaxHandler;

    @Override
    public void getContents(Page page) {
        if (page.getOriginalAddress() == null) {
            throw new RuntimeException("URL is null, skipping");
        }
        try {
            pageSaxHandler.setPage(page);
            SAXParserImpl.newInstance(null).parse(
                    comms.getInputStream(page.getOriginalAddress()),
                    pageSaxHandler
            );
        } catch (Exception e) {
            LOGGER.error("Error reading page: {}", e.getMessage(), e);
            throw new RuntimeException("Error reading page: " + e.getMessage(), e);
        }
    }
}
