package uk.divisiblebyzero.kasscraper.service;

import uk.divisiblebyzero.kasscraper.domain.Page;

/**
 * Created by: Matthew Smalley
 * Date: 20/12/2015
 */
public interface PageWriterService {
    void writePage(Page p);
}
