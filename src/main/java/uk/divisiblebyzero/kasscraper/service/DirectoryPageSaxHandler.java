package uk.divisiblebyzero.kasscraper.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import uk.divisiblebyzero.kasscraper.domain.Page;

import javax.annotation.Resource;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by: Matthew Smalley
 * Date: 16/12/2015
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class DirectoryPageSaxHandler extends DefaultHandler {
    private boolean listening;
    private String href;
    private StringBuilder textBuilder;
    @Value("${hrefRegex:MIs.*htm}")
    private String hrefRegex;
    private URL currentBaseURL;

    @Resource
    private ScrapingDirectoryService scrapingDirectoryService;
    private String currentCategory;

    public void setCurrentBaseURL(URL currentBaseURL) {
        this.currentBaseURL = currentBaseURL;
    }

    public void startElement(String uri, String localName,
                             String name, Attributes a) {
        if (name.equalsIgnoreCase("a") && a.getValue("href") != null && a.getValue("href").matches(hrefRegex)) {
            listening = true;
            textBuilder = new StringBuilder();
            href = a.getValue("href");
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName)
            throws SAXException {
        if (localName.equalsIgnoreCase("a") && listening) {
            listening = false;
            Page page = new Page();
            page.setTitle(textBuilder.toString());
            page.setParentCategory(currentCategory);
            try {
                page.setOriginalAddress(new URL(currentBaseURL, href));
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            scrapingDirectoryService.postNewPage(page);
        }
    }

    public void characters(char ch[], int start, int length) throws SAXException {
        if (listening) {
            textBuilder.append(new String(ch, start, length).trim().replaceAll("\n", ""));
        }
    }


    public void setCurrentCategory(String currentCategory) {
        this.currentCategory = currentCategory;
    }

    public String getCurrentCategory() {
        return currentCategory;
    }
}
