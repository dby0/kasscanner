package uk.divisiblebyzero.kasscraper.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import uk.co.divisiblebyzero.wordpress.Post;
import uk.co.divisiblebyzero.wordpress.WordPressRpcClient;
import uk.divisiblebyzero.kasscraper.domain.Page;

import javax.annotation.Resource;

/**
 * Created by: Matthew Smalley
 * Date: 20/12/2015
 */
@Service
public class WordPressPageWriterService implements PageWriterService {
    private final static Logger LOGGER = LoggerFactory.getLogger(WordPressPageWriterService.class);
    private boolean loggedIn = false;

    @Value("${wordpress.username:kasscraper}")
    private String username;
    @Value("${wordpress.password:PASSWORD}")
    private String password;
    @Value("${wordpress.rpcurl:http://alpha.divisiblebyzero.co.uk/xmlrpc.php}")
    //@Value("${wordpress.rpcurl:http://192.168.99.100:8080/xmlrpc.php}")
    private String rpcurl;

    @Resource
    private WordPressRpcClient client;

    public void writePage(Page p) {
        LOGGER.info("Writing page: {}", p);
        if (p.getTitle() == null || "".equals(p.getTitle().trim())) {
            LOGGER.error("Skipping blank page");
            return;
        }
        if (p.getContents() == null || "".equals(p.getContents().trim())) {
            LOGGER.error("Skipping blank contents");
            return;
        }
        Post post = new Post();
        post.setPostTitle(p.getTitle());
        post.setContents(p.getContents());
        post.getCategories().add(p.getParentCategory());

        post = client.addPost(post);
        p.setDestinationId(post.getPostId());
        LOGGER.info("Page written: {}", p);

        if (p.getImages().keySet().size() > 0) {
            LOGGER.info("Images found ... uploading");
            for (String imageName : p.getImages().keySet()) {
                client.uploadImage(imageName, p.getImages().get(imageName), p.getDestinationId());
            }
        }
    }

}
