package uk.divisiblebyzero.kasscraper.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import uk.divisiblebyzero.kasscraper.domain.Page;

import javax.annotation.Resource;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by: Matthew Smalley
 * Date: 20/12/2015
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class PageSaxHandler extends DefaultHandler {
    private boolean listening;
    private StringBuilder textBuilder;
    private Page page;
    private int refCount = 0;

    public void setPage(Page page) {
        this.page = page;
    }

    public void startElement(String uri, String localName,
                             String name, Attributes a) {
        if (localName.equalsIgnoreCase("table")) {
            if (listening) {
                refCount++;
            } else {
                if ("0".equals(a.getValue("border"))) {
                    listening = true;
                    textBuilder = new StringBuilder();
                }
            }
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName)
            throws SAXException {
        if (localName.equalsIgnoreCase("table") && listening) {
            if (refCount == 0) {
                listening = false;
                page.setContents(textBuilder.toString());
            } else {
                refCount--;
            }
        }
    }

    public void characters(char ch[], int start, int length) throws SAXException {
        if (listening) {
            textBuilder.append(new String(ch, start, length).trim().replaceAll("\n", ""));
            textBuilder.append(" ");
        }
    }
}