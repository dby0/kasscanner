package uk.divisiblebyzero.kasscraper.service;

import uk.divisiblebyzero.kasscraper.domain.Page;

import java.net.URL;
import java.util.Map;
import java.util.Set;

/**
 * Created by: Matthew Smalley
 * Date: 14/12/2015
 */
public interface DirectoryService {
    Set<Page> getPageLocations();

    void postNewPage(Page newPage);
}
