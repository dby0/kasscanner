package uk.divisiblebyzero.kasscraper.service;

import org.springframework.stereotype.Service;
import uk.divisiblebyzero.kasscraper.domain.Page;

import javax.annotation.Resource;
import java.util.Set;

/**
 * Created by: Matthew Smalley
 * Date: 20/12/2015
 */
@Service
public class KasScraperService {
    @Resource
    private DirectoryService directoryService;

    @Resource
    private PageService pageService;

    @Resource
    private PageWriterService pageWriterService;

    public void scrape() {
        Set<Page> pages = directoryService.getPageLocations();
        int i = 0;
        for (Page p: pages) {
            if (i > 1000) {
                break;
            }
            pageService.getContents(p);
            pageWriterService.writePage(p);

            i++;
        }
        // figure out cross-links
    }

}
