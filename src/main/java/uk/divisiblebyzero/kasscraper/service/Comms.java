package uk.divisiblebyzero.kasscraper.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.Charset;

/**
 * Created by: Matthew Smalley
 * Date: 14/12/2015
 */
@Service
public class Comms {
    private final static Logger LOGGER = LoggerFactory.getLogger(Comms.class);

    protected Charset getCharset() {
        return Charset.forName("Cp1252");
    }

    public String cleanFilename(String filename) {
        return filename.replaceAll("[/\\:]", "-");
    }

    public InputStream getInputStream(URL url) {
        try {
            return url.openConnection().getInputStream();
        } catch (IOException e) {
            LOGGER.error("Error reading url: {}", e.getMessage(), e);
            return null;
        }
    }

    public InputStreamReader getInputStreamReader(URL url) {
        try {
            return new InputStreamReader(url.openConnection().getInputStream(), getCharset());
        } catch (IOException e) {
            LOGGER.error("Error reading url: {}", e.getMessage(), e);
            return null;
        }
    }
}
