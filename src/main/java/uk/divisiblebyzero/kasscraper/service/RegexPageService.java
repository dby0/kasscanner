package uk.divisiblebyzero.kasscraper.service;

import org.apache.commons.io.IOUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import uk.divisiblebyzero.kasscraper.domain.Page;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

/**
 * Created by: Matthew Smalley
 * Date: 21/12/2015
 */
@Service("pageService")
public class RegexPageService implements PageService {
    private final static Logger LOGGER = LoggerFactory.getLogger(RegexPageService.class);

    private final static class RegexInstruction {
        private String search;
        private String replace;
        private boolean repeat;

        public RegexInstruction(String search, String replace, boolean repeat) {
            this.search = search;
            this.replace = replace;
            this.repeat = repeat;
        }
    }

    private final static RegexInstruction[] REGEXES = new RegexInstruction[]{
            new RegexInstruction("\\r\\n", " ", true),
            new RegexInstruction("<div[^>]*>.*?</div>", "", false),
            new RegexInstruction("<br>", "<br></br>", true),
            new RegexInstruction("<[^>]*font[^>]*>", "", true),
            new RegexInstruction("width=\"?[0-9]*\"?", "", true),
            new RegexInstruction("style=\"[^\"]*\"", "", true),
            new RegexInstruction("style='[^']*'", "", true),
            new RegexInstruction("<[^>]*blockquote[^>]*>", "", true),
            new RegexInstruction("For details about the advantages of membership.*", "", false),
            new RegexInstruction("Back\\s*to Churchyards listed</a></p>\\s*</center>", "Back to Churchyards listed</a></p></td></tr></table></center>", false),
            new RegexInstruction("’", "'", true)
    };


    @Resource
    private Comms comms;

    protected String chopBody(String input) {
        String capitals = input.toUpperCase();
        int closeBodyStart = capitals.indexOf("</BODY");

        int openBodyStart = capitals.indexOf("<BODY");
        int openBodyEnd = -1;
        if (openBodyStart != -1) {
            openBodyEnd = capitals.indexOf(">", openBodyStart);
        }
        if (closeBodyStart == -1) {
            return input.substring(openBodyEnd + 1);
        } else {
            return input.substring(openBodyEnd + 1, closeBodyStart);
        }

    }

    @Override
    public void getContents(Page page) {
        LOGGER.info("Downloading page: {}", page.getOriginalAddress());
        InputStreamReader is = comms.getInputStreamReader(page.getOriginalAddress());
        try {
            LOGGER.info("Parsing page: {}", page.getOriginalAddress());
            String newHtml = IOUtils.toString(is);
            newHtml = chopBody(newHtml);
            for (RegexInstruction r : REGEXES) {
                if (r.repeat) {
                    newHtml = newHtml.replaceAll(r.search, r.replace);
                } else {
                    newHtml = newHtml.replaceFirst(r.search, r.replace);
                }
            }
            boolean imagesFound = false;
            Element e = Jsoup.parse(newHtml).body();
            for (Element e2 : e.getElementsByTag("img")) {
                imagesFound = true;
                URL imageUrl = new URL(page.getOriginalAddress(), e2.attr("src"));
                String filteredName = comms.cleanFilename(imageUrl.toString());
                page.getImages().put(filteredName, IOUtils.toByteArray(imageUrl));
                e2.attr("src", filteredName);
            }


            newHtml = e.html();
            if (imagesFound) {
                newHtml = "[gallery]" + newHtml;
            }

            LOGGER.info("Parse complete");
            page.setContents(newHtml);
        } catch (IOException e) {
            LOGGER.error("Error reading page: {}", e.getMessage(), e);
            throw new RuntimeException("Error reading page: " + e.getMessage(), e);
        }
    }
}
