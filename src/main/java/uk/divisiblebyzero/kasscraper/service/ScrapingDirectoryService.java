package uk.divisiblebyzero.kasscraper.service;

import org.ccil.cowan.tagsoup.jaxp.SAXParserImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;
import uk.divisiblebyzero.kasscraper.domain.Page;

import javax.annotation.Resource;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by: Matthew Smalley
 * Date: 13/12/2015
 */
@Service
public class ScrapingDirectoryService implements DirectoryService {
    private final static Logger LOGGER = LoggerFactory.getLogger(ScrapingDirectoryService.class);
    @Resource
    private Comms comms;

    @Resource
    private DirectoryPageSaxHandler directoryPageSaxHandler;

    private String startUrl = "http://www.kentarchaeology.org.uk/Research/Libr/MIs/MIslist.htm";
    private String category = "Churchyard Monumental Inscriptions";

    private Set<Page> pageLocations;

    private URL currentBaseURL;

    public void scrapeDirectory(String directoryUrl) throws IOException, XPathExpressionException, ParserConfigurationException, SAXException {
        currentBaseURL = new URL(directoryUrl);
        directoryPageSaxHandler.setCurrentBaseURL(currentBaseURL);
        directoryPageSaxHandler.setCurrentCategory(category);
        SAXParserImpl.newInstance(null).parse(
                comms.getInputStream(currentBaseURL),
                directoryPageSaxHandler
        );
    }

    @Override
    public Set<Page> getPageLocations() {
        try {
            pageLocations = new HashSet<>();
            scrapeDirectory(startUrl);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
        return pageLocations;
    }

    @Override
    public void postNewPage(Page newPage) {
        LOGGER.info("Adding new link: {}", newPage);
        if (pageLocations.contains(newPage)) {
            LOGGER.error("Skipping link for article {} since it already exists", newPage);
            return;
        }
        pageLocations.add(newPage);
    }
}
