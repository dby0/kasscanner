package uk.divisiblebyzero.kasscraper.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import uk.co.divisiblebyzero.wordpress.WordPressRpcClient;
import uk.divisiblebyzero.kasscraper.service.ServicePackage;

/**
 * Created by: Matthew Smalley
 * Date: 17/11/2015
 */
@Configuration
@ComponentScan(basePackageClasses = {ServicePackage.class, SpringConfig.class})
@PropertySource("classpath:/kasscraper.properties")
public class SpringConfig {
    @Value("${wordpress.username:kasscraper}")
    private String username;
    @Value("${wordpress.password:PASSWORD}")
    private String password;
    @Value("${wordpress.rpcurl:http://alpha.divisiblebyzero.co.uk/xmlrpc.php}")
    //@Value("${wordpress.rpcurl:http://192.168.99.100:8080/xmlrpc.php}")
    private String xmlrpcUrl;

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean
    public WordPressRpcClient wordPressRpcClient() {
        WordPressRpcClient client = new WordPressRpcClient();
        client.setUsername(username);
        client.setPassword(password);
        client.setXmlrpcUrl(xmlrpcUrl);
        return client;
    }
}

