package uk.divisiblebyzero.kasscraper.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import uk.divisiblebyzero.kasscraper.service.Comms;
import uk.divisiblebyzero.kasscraper.service.LocalComms;

/**
 * Created by: Matthew Smalley
 * Date: 14/12/2015
 */
@Configuration
@Profile("LOCALCOMMS")
public class LocalCommsConfig {
    @Bean
    public Comms comms() {
        return new LocalComms();
    }
}
