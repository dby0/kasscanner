package uk.divisiblebyzero.kasscraper.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;

/**
 * Created by: Matthew Smalley
 * Date: 14/12/2015
 */
@Profile({"LOCALCOMMS","UNIT"})
@Configuration
@Import({SpringConfig.class, LocalCommsConfig.class})
public class UnitTestConfig {
}
