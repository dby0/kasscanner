package uk.divisiblebyzero.kasscraper.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import uk.divisiblebyzero.kasscraper.config.UnitTestConfig;

import javax.annotation.Resource;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by: Matthew Smalley
 * Date: 14/12/2015
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader=AnnotationConfigContextLoader.class, classes={UnitTestConfig.class})
@ActiveProfiles({"LOCALCOMMS"})
public class TestLocalComms {
    @Resource
    private Comms comms;
    @Test
    public void testItsTheRightOne() throws MalformedURLException {
        Assert.assertTrue(comms instanceof LocalComms);
        comms.getInputStreamReader(new URL("http://www.kentarchaeology.org.uk/Research/Libr/MIs/MIslist.htm"));
    }
}
