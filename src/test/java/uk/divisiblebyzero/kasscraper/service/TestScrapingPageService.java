package uk.divisiblebyzero.kasscraper.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import uk.divisiblebyzero.kasscraper.config.UnitTestConfig;
import uk.divisiblebyzero.kasscraper.domain.Page;

import javax.annotation.Resource;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by: Matthew Smalley
 * Date: 20/12/2015
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader=AnnotationConfigContextLoader.class, classes={UnitTestConfig.class})
@ActiveProfiles({"LOCALCOMMS"})
public class TestScrapingPageService {
    @Resource
    private ScrapingPageService scrapingPageService;
    @Test
    public void testScrapingPageService() throws MalformedURLException {
        Page p = new Page();
        p.setOriginalAddress(new URL("http://www.kentarchaeology.org.uk/Research/Libr/MIs/MIsBlean/01.htm"));
        scrapingPageService.getContents(p);
        Assert.assertNotNull(p.getContents());
        System.out.println(p.getContents());
    }
}
