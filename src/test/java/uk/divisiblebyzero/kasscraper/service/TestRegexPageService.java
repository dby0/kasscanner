package uk.divisiblebyzero.kasscraper.service;

import org.apache.commons.io.IOUtils;
import org.ccil.cowan.tagsoup.jaxp.SAXParserImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import uk.divisiblebyzero.kasscraper.config.UnitTestConfig;
import uk.divisiblebyzero.kasscraper.domain.Page;

import javax.annotation.Resource;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Stack;

import static org.junit.Assert.assertEquals;

/**
 * Created by: Matthew Smalley
 * Date: 20/12/2015
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class, classes = {UnitTestConfig.class})
@ActiveProfiles({"LOCALCOMMS"})
public class TestRegexPageService {
    @Resource
    private RegexPageService regexPageService;

    private final static class VerifyingSaxHandler extends DefaultHandler {

        private Stack<String> elements = new Stack<>();

        @Override
        public void startElement(String uri, String localName,
                                 String name, Attributes a) {
            elements.push(localName);
        }

        @Override
        public void endElement(String uri, String localName, String qName)
                throws SAXException {
            if (elements.empty()) {
                throw new RuntimeException(String.format("Error: run out of elements for end tag %s", localName));
            }
            if (!elements.peek().equals(localName)) {
                throw new RuntimeException(String.format("Error: element end tag of %s does not match stacked element %s", localName, elements.peek()));
            } else {
                elements.pop();
            }
        }
    }

    private void verifyValidFragment(String fragment) {
        try {
            SAXParserFactory spf = SAXParserFactory.newInstance();
            spf.setNamespaceAware(false);
            spf.setValidating(false);

            SAXParser saxParser = spf.newSAXParser();
            XMLReader xmlReader = saxParser.getXMLReader();
            xmlReader.setContentHandler(new VerifyingSaxHandler());
//            xmlReader.parse(new InputSource(new StringReader("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">" + fragment)));

            String header = "<!DOCTYPE html [\n" +
                    "    <!ENTITY nbsp \"&#160;\"> \n" +
                    "]>";
            xmlReader.parse(new InputSource(new StringReader(header + fragment)));
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
    }

    private void verifyValidFragment2(String fragment) {
        try {
            SAXParserImpl.newInstance(null).parse(
                    new ByteArrayInputStream(fragment.getBytes()),
                    new VerifyingSaxHandler()
            );
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void testImageHandling() throws Exception {
        Page p = new Page();
        p.setOriginalAddress(new URL("http://www.kentarchaeology.org.uk/Research/Libr/MIs/MIsCantStDunstans/01.htm"));
        regexPageService.getContents(p);
        System.out.println(p.getContents());
    }

    @Test
    public void testChopBody() {

        assertEquals("inputstring", regexPageService.chopBody("inputstring"));
        assertEquals("inputstring", regexPageService.chopBody("adsjas<body>inputstring"));
        assertEquals("inputstring", regexPageService.chopBody("inputstring</body>asdjhasjdk"));
        assertEquals("inputstring", regexPageService.chopBody("asdhajsd<body asda>inputstring</body>asdjhasjdk"));

        String input = "adjashdjs adjkasj ajsdkasjk< body> adsjsakdjk <bodY asdjfkd=\"asjdfksj\" >ajsd<asdj>ajsdasjdk<bodyasdjkasd></asdjksadjkbody></body>";
        String output = regexPageService.chopBody(input);
        assertEquals("ajsd<asdj>ajsdasjdk<bodyasdjkasd></asdjksadjkbody>", output);
    }

    @Test
    public void testLongPage() throws Exception {
        Page p = new Page();
        p.setOriginalAddress(new URL("http://www.kentarchaeology.org.uk/Research/Libr/MIs/MIsHighHalden/High%20Halden%20churchyard%20M_I_'s.htm"));
        regexPageService.getContents(p);
    }

    @Test
    public void testIt() throws IOException {
        Charset charset;
        charset = Charset.forName("Cp1252");
        InputStreamReader isr = new InputStreamReader(TestRegexPageService.class.getResourceAsStream("/resources/http---www.kentarchaeology.org.uk-Research-Libr-MIs-MIsDealStLeonard-01.htm"), charset);
        String input = IOUtils.toString(isr);
        input = input.replaceAll("\\r\\n", " ");
        input = input.substring(input.indexOf("Mannor of Bulstrode") + 30, input.indexOf("Mannor of Bulstrode") + 60);

        for (char i : input.toCharArray()) {
            System.out.println((int) i);
        }
        System.out.println(input);
    }

    @Test
    public void testScrapingPageService() throws MalformedURLException {
        Page p = new Page();
        p.setOriginalAddress(new URL("http://www.kentarchaeology.org.uk/Research/Libr/MIs/MIsDealStLeonard/01.htm"));
        regexPageService.getContents(p);
        Assert.assertNotNull(p.getContents());
        System.out.println(p.getContents());


        verifyValidFragment2(p.getContents());

        verifyValidFragment2("<p>This is some<center>text</p>");
    }
}
