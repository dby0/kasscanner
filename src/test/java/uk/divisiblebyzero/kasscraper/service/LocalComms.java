package uk.divisiblebyzero.kasscraper.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

/**
 * Created by: Matthew Smalley
 * Date: 14/12/2015
 */
public class LocalComms extends Comms {
    private final static Logger LOGGER = LoggerFactory.getLogger(LocalComms.class);

    @Override
    public InputStream getInputStream(URL url) {
        String urlString = url.toString();
        if (urlString == null || !urlString.endsWith(".htm")) {
            LOGGER.error("Unknown url pattern {}, bailing", urlString);
            throw new RuntimeException("Unknown url pattern : " + urlString + ", bailing");
        }
        String newPattern = cleanFilename(urlString);
        LOGGER.info("Grabbing url: {} as {}", urlString, newPattern);
        return LocalComms.class.getResourceAsStream("/resources/" + newPattern);
    }

    @Override
    public InputStreamReader getInputStreamReader(URL url) {
        return new InputStreamReader(getInputStream(url), getCharset());
    }
}
