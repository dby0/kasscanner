package uk.divisiblebyzero.kasscraper.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import uk.divisiblebyzero.kasscraper.config.UnitTestConfig;
import uk.divisiblebyzero.kasscraper.domain.Page;

import javax.annotation.Resource;
import java.net.URL;
import java.util.Map;
import java.util.Set;

/**
 * Created by: Matthew Smalley
 * Date: 14/12/2015
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader=AnnotationConfigContextLoader.class, classes={UnitTestConfig.class})
@ActiveProfiles({"LOCALCOMMS"})
public class TestScrapingDirectoryService {
    @Resource
    private DirectoryService directoryService;
    @Test
    public void testScanningDirectoryService() {
        Set<Page> pages = directoryService.getPageLocations();
        Assert.assertTrue(pages.size() > 0);
        for (Page p: pages) {
            Assert.assertNotNull(p);
            Assert.assertNotNull(p.getTitle());
            Assert.assertNotNull(p.getOriginalAddress());
        }
    }
}
