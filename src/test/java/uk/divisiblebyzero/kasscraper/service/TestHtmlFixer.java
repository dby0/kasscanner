package uk.divisiblebyzero.kasscraper.service;

import org.ccil.cowan.tagsoup.jaxp.SAXParserImpl;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.junit.Test;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.io.ByteArrayInputStream;
import java.io.IOException;

/**
 * Created by: Matthew Smalley
 * Date: 23/12/2015
 */
public class TestHtmlFixer {
    private String fragment = "<p>This is some<center>text</p></center>";

    @Test
    public void testJSoupFixer() {
        Document doc = Jsoup.parse(fragment);
        Element body = doc.body();
        System.out.println(doc.body().html());


    }

    @Test
    public void testSoup() throws SAXException, IOException {
        SAXParserImpl.newInstance(null).parse(
                new ByteArrayInputStream(fragment.getBytes()),
                new DefaultHandler() {
                    public void startElement(String uri, String localName,
                                             String name, Attributes a) {
                        if (name.equalsIgnoreCase("img"))
                            System.out.println(a.getValue("src"));
                    }
                }
        );
    }


}
